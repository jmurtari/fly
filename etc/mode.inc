<?  /* Copyright (C) 1995-2020  John Murtari
This file is part of FLY flight management software
FLY is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLY is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLY.  If not, see <https://www.gnu.org/licenses/>. */ ?>
<?

define ("REAL_MODE", 1);

if (REAL_MODE) {  // the real system
  
  $NoUseDutySched = '';
  // $NoUseDutySched = "<p><font color='red'><i>This feature of the system is disabled for this installation.  Other tools are used to schedule airport duty assignments. Aircraft scheduling is still supported.</i></font></p>";  // uncomment to not use flying day duty scheduling, still allows reservations

  define ("ATTACH_DIR", "attachments");
  define ("BUILD_ROOT", "");

  // You MUST CHANGE THE FOLLOWING
  define ("CLUB_NAME", 'Generic Club');
  define ("ROOT_DIR", "/pub/comwww/soaringtools");  // absolute path to top of web tree for main web site

  define ("FLY_DIR", '');  // just the install directory, beneath ROOT_DIR above
  define ("FAC_HOST", 'localhost'); // DB host, stay local for the best security
  define ("FAC_DB", '');   // DB name (these three must all be different, at least 8 characters)
  define ("DB_USER", '');  // DB username (don't cheat SW will check all)
  define ("DB_PASS", '');  // DB password (must include 1 uppercase, 1 numeric, 1 special) 

  // these are local to your website/ email addrs, remove fly or www.fly and replace with your values
  define ("MAIL_LIST_EMAIL", "memberlist@fly.org");
  define ("NO_REPLIES_EMAIL", "no_replies_please@fly.org");
  define ("NON_SECURE_URL", "http://www.fly.org/");
  define ("SECURE_URL", "https://www.fly.org/");
  define ("SERVICE_EMAIL","webmaster@fly.org");
  if (isset($_SERVER['HTTPS'])) {
    define ("SITE_URL", "https://www.fly.org");
  } else {
    define ("SITE_URL", "");
  }

  define ("STAFF_CHECK", 'XXXXXXXXXXXXXXXXXXXX');

  // we define some layout items that are always club specific
  $CheckoutTypes = Array('ASK 21', '1-26', '1-34', '2-33', 'Discus CS', 'Duo Discus', 'Pawnee', 
                         'Supercub', 'Pawnee Tow', 'Supercub Tow', 'Cash', 'CFIG', 'Com', 'Line', 
                         'Log', 'Wing Runner');
    
  # Duty Names - for airfield job assignment the system assumes two things.  
  # One or more coordinators are assigned to supervise people
  # performing a duty, e.g. "TOW" coordinator manages all the Tow pilots.
  # This is defined in config.inc as $CoordTypes and also for the coord ENUM column in the db.
  # NEXT
  # A club could have a different tow pilot assigned for a morning shift, vs. afternoon, we
  # need to correlate those names with the coordinator so tracking/alerting can happen.
  # Duty table, type column must agree, e.g.
  # ALTER TABLE Duty MODIFY COLUMN type ENUM('AM Cash', 'AM Com',  'AM Log', 'AM Tow', 
  #                  'AM Line', 'AM CFIG', 'PM Cash', 'PM Com', 'PM Log', 'PM Tow', 'PM Line', 'PM CFIG');
  if (0) { //ZZ testing
      $DutyName = array('AM Cash' => 'CASH', 'AM Com' => 'COM', 'AM Log' => 'LOG', 'AM Tow' => 'TOW', 
                    'AM Line' => 'LINE', 'AM CFIG' => 'CFIG',
                    'PM Cash' => 'CASH', 'PM Com' => 'COM', 'PM Log' => 'LOG', 'PM Tow' => 'TOW', 
                    'PM Line' => 'LINE', 'PM CFIG' => 'CFIG'); 
  } else {
      $DutyName = array('AM Cash' => 'CASH', 'AM Com' => 'COM', 'AM Log' => 'LOG', 'AM Tow' => 'TOW'); 
  }
      
  // DUTY CONTACT POINTS - used in alert CRON job, manageSched
  // ZZ - should come from DB
  $DutyContact = Array('OPS'  => 'ops@fly.org',
                     'TOW'  => 'two@fly.org',
                     'CFIG' => 'cfig@fly.org',
                     'CREW' => 'crew@fly.org',
                     'SCHEDA' => 'scheda@fly.org',
                     'SCHEDB' => 'schedb@fly.org',
                     'BOARD'  => 'board@fly.org');



  # The membership types you want, YOU MUST LEAVE: 'Pending Delete' and 'None' as is, special use by the system.
  $MemberTypes = array('Associate Active', 'Associate Limited', 'College Junior', 'Junior', 'Senior Active', 'Other', 'Inactive', 'None');

  # What sort options do you want on the membership page
  $SortMemberOptions = array('Select', 'Biennial Flt Review', 'City', 'Coordinator', 'E-Mail Addr', 'Glider Rating', 'Last Name', 'Member Since', 'Membership Type', 'Mentor', 'Tow Medical');

    # For High-Far-Long (hfl)
    # for LAT/LON you want the middle of the field, between takeoff and landing points.
    define('AIRPORT_NAME', 'Harris Hill(4NY8)');  // 4NY8 - Harris Hill, Elmira, NY
    define('AIRPORT_LAT', '42.119522');  
    define('AIRPORT_LON', '-76.900490');
    define('AIRPORT_ELEV', '1710'); // feet
    define('HFL_UNITS', 'US');
    define('HFL_TIMEZONE', -240);  // minutes offset from UTC
    
    # NOTE - if you add an aircraft, make sure to modify the ENUM for the glider column
    # in the hfl table!
    $HFL_GLIDERS = array('ASK 21','Discus CS','Duo Discus','G 102','L-13','SGS 1-26A','SGS 2-33A','SGS 1-34');
  
  
 } else {  // we are testing
  
 // echo "Bad choice";
 //exit(1);
  
 }  // end if-else real mode

?>
