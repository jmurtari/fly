#!/bin/bash

USER="fly"
PASSWORD='fly!1903'

databases=`mysql -u $USER -p$PASSWORD -e "SHOW DATABASES;" | tr -d "| " | grep -v Database`
PASSWORD='Wright!1903'

for db in $databases; do
    if [[ "$db" != "information_schema" ]] && [[ "$db" != "performance_schema" ]] && [[ "$db" != "mysql" ]] && [[ "$db" != _* ]] ; then
        echo "Dumping database: $db"
        mysqldump -u $USER -p$PASSWORD --opt --databases $db | gzip > `date +%m%d`.$db.sql.gz
       # gzip $OUTPUT/`date +%Y%m%d`.$db.sql
    fi
done
